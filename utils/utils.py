import numpy as np
from scipy.io.wavfile import read


def read_wav_np(path):
    sr, wav = read(path)

    if len(wav.shape) == 2:
        wav = wav[:, 0]

    if wav.dtype == np.int16:
        wav = wav / 32768.0
    elif wav.dtype == np.int32:
        wav = wav / 2147483648.0
    elif wav.dtype == np.uint8:
        wav = (wav - 128) / 128.0

    wav = wav.astype(np.float32)

    return sr, wav

class WavBinaryWrapper(object):
    def __init__(self, data):
        super(WavBinaryWrapper, self).__init__()
        self.data = data
        self.position = 0

    def read(self, n=-1):
        if n == -1:
            data = self.data[self.position:]
            self.position = len(self.data)
            return data
        else:
            data = self.data[self.position:self.position+n]
            self.position += n
            return data

    def seek(self, offset, whence=0):
        if whence == 0:
            self.position = offset
        elif whence == 1:
            self.position += offset
        elif whence == 2:
            self.position = len(self.data) + offset
        else:
            raise RuntimeError('test')
        return self.position

    def tell(self):
        return self.position

    def close(self):
        pass
