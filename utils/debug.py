from PIL import Image
import numpy
import colorsys

from sklearn.manifold import TSNE

def draw_points(matrix, gt=None):
    """
    # PCA
    e = numpy.matmul(matrix.T, matrix)
    v, w = numpy.linalg.eigh(e)
    simpl_mat = numpy.matmul(matrix, w[:, -2:]).tolist()
    """
    # TSNE
    tsne = TSNE(n_components=2)
    simpl_mat = tsne.fit_transform(matrix)
    mx = numpy.max(numpy.sqrt(numpy.sum(simpl_mat * simpl_mat, axis=1)))
    simpl_mat /= mx
    simpl_mat = simpl_mat.tolist()
    del mx
    # """

    if gt is None:
        gt = [0] * len(simpl_mat)
        colors = [(0, 0, 0, 255)]
    else:
        color_count = len(set(gt))
        colors = [(lambda r,g,b:(int(r*255),int(g*255),int(b*255),255))(*colorsys.hsv_to_rgb(i / color_count, 1, 1)) for i in range(color_count)]
        del color_count

    im = Image.new('RGBA', (2001, 2001), (255, 255, 255, 255))
    pix = im.load()
    for i in range(0, 2000):
        pix[1000,i] = (128, 128, 128, 255)
        pix[i, 1000] = (128, 128, 128, 255)
    for i,x in enumerate(simpl_mat):
        u,v = x
        u = int(1000 * u + 1000)
        v = int(1000 * v + 1000)
        color = colors[gt[i]]
        if not (0 <= u <= 2000 and 0 <= v <= 2000):
            color = (color[0], color[1], color[2], 100)
            u = max(0, min(2000, u))
            v = max(0, min(2000, v))
        for dx in range(-4, 5):
            for dy in range(-4, 5):
                uu = max(0, min(2000, u + dx))
                vv = max(0, min(2000, v + dy))
                pix[uu,vv] = color
    im.save('result.png')
