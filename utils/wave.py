import struct


class WavContent():
    def __init__(self, copy=None):
        if copy is None:
            self._data = None
        else:
            try:
                self.sc1_size = copy.sc1_size
                self.audio_format = copy.audio_format
                self.num_channels = copy.num_channels
                self.sample_rate = copy.sample_rate
                self.bits_per_sample = copy.bits_per_sample
                self._data = copy._data
            except:
                self._data = None

    def from_bytes(self, data):
        if data[0:4] != b'RIFF' and data[8:12] != b'WAVE':
            raise ValueError('not a wav data, header mismatch')
        (total_size,) = struct.unpack('<I', data[4:8])
        if total_size + 8 != len(data):
            raise ValueError('not a wav data, total size mismatch')
        total_size += 8

        pos = 12
        while pos < total_size:
            now = data[pos : pos + 4]
            pos += 4
            if now == b'fmt ':
                self._from_fmt(data[pos : pos + 20])
                pos += 20
            elif now in [b'LIST', b'_PMX', b'DISP', b'id3 ']:
                (info_size,) = struct.unpack('<I', data[pos : pos + 4])
                # TODO: parse LIST!
                pos += 4 + info_size
            elif now == b'data':
                (data_size,) = struct.unpack('<I', data[pos : pos + 4])
                if self._data is None:
                    self._data = data[pos + 4 : pos + 4 + data_size]
                else:
                    raise ValueError('two or more data chunks are given')
                pos += 4 + data_size
            else:
                raise ValueError('"%s"' % "".join('%02x' % x for x in now))

        if pos > total_size:
            raise ValueError('not enough values to decode')

    def to_bytes(self):
        header = struct.pack('<IIIIIHHIIHHII',
            0x46464952,
            36 + len(self._data),
            0x45564157,
            0x20746d66,
            self.sc1_size,
            self.audio_format,
            self.num_channels,
            self.sample_rate,
            self.sample_rate * self.num_channels * self.bits_per_sample // 8,
            self.num_channels * self.bits_per_sample // 8,
            self.bits_per_sample,
            0x61746164,
            len(self._data)
        )
        return header + self._data

    def _from_fmt(self, data):
        (
            self.sc1_size,
            self.audio_format,
            self.num_channels,
            self.sample_rate,
            byte_rate,
            block_align,
            self.bits_per_sample
        ) = struct.unpack('<IHHIIHH', data)

        if self.sc1_size != 16 or self.audio_format != 1:
            raise NotImplementedError('the wav is not a pcm')

        if byte_rate * 8 != self.sample_rate * self.num_channels * self.bits_per_sample:
            raise ValueError('not a wav data, byte rate mismatch')

        if block_align * 8 != self.num_channels * self.bits_per_sample:
            raise ValueError('not a wav data, block align mismatch')

    def __getitem__(self, key):
        if isinstance(key, slice):
            if key.step is not None:
                raise ValueError('you cannot sample with step')
            s,e = key.start, key.stop
            if s is None:
                s = 0
            if e is None:
                e = len(self._data) // self.sample_rate
            res = WavContent(self)
            bytes_per_sample = self.bits_per_sample // 8
            res._data = self._data[s * bytes_per_sample : e * bytes_per_sample]
            return res
        raise ValueError('you cannot sample one frame')

    def __len__(self):
        return len(self._data) // (self.bits_per_sample // 8)

    def len_in_seconds(self):
        return len(self) / self.sample_rate

    def __iadd__(self, other):
        if self.bits_per_sample != other.bits_per_sample:
            raise ValueError('bits per sample mismatch')
        self._data += other._data
        return self
