import os
import grpc
import time
import torch
import logging
import argparse
import numpy as np

from omegaconf import OmegaConf
from concurrent import futures
from spectralcluster import SpectralClusterer

from w2l_pb2_grpc import SpeechToTextStub

from dap_pb2_grpc import GhostVLADStub

from diarize_pb2 import Speech, Segment, DiarizeResult
from diarize_pb2_grpc import DiarizeServicer, add_DiarizeServicer_to_server

from utils.wave import WavContent
# from utils import debug
from utils.utils import read_wav_np, WavBinaryWrapper

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class DiarizationImpl(DiarizeServicer):
    def __init__(self, stt_remote, vr_remote, config_path, chunk_size=64*1024):
        self.stt_remote = stt_remote
        self.vr_remote = vr_remote
        self.chunk_size = chunk_size

        hp = OmegaConf.load(config_path)
        self.sampling_rate = hp.sampling_rate # for time calculation
        self.clusterer = SpectralClusterer(
            min_clusters=hp.min_clusters,
            max_clusters=hp.max_clusters,
            p_percentile=hp.p_percentile,
            gaussian_blur_sigma=hp.gaussian_blur_sigma,
        )

    def StreamDiarization(self, message, context):
        try:
            with grpc.insecure_channel(self.stt_remote) as stt_channel, \
                grpc.insecure_channel(self.vr_remote) as vr_channel:
                return self._get_diarization(stt_channel, vr_channel, message, context)

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    def _get_diarization(self, stt_channel, vr_channel, message, context):
        stt_stub = SpeechToTextStub(stt_channel)
        vr_stub = GhostVLADStub(vr_channel)

        matrix = list()
        timestamp = list()

        ''' to extract segment of speech for Speaker Verification,
        all message chunks should be received before stream_recognize.'''
        whole_wav = bytearray()
        for wav_chunk in message:
            whole_wav.extend(wav_chunk.bin)

        wav_binary = WavBinaryWrapper(whole_wav)
        wav_content = WavContent()
        wav_content.from_bytes(bytes(wav_binary.read()))

        wav_binary = WavBinaryWrapper(whole_wav)
        sr, wav = read_wav_np(wav_binary)
        if sr != self.sampling_rate:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details("sampling rate mismatch: expected %d, got %d" \
                % (sr, self.sampling_rate))
            return None

        wav_iterator = self._generate_wav_binary_iterator(wav_content._data)
        for pcm_idx, segment in enumerate(stt_stub.StreamRecognize(wav_iterator)):
            data = wav_content[segment.start : segment.end]
            msg = segment.txt.strip()
            if msg:
                short_data = data
                if short_data.len_in_seconds() < 2.5:
                    r = WavContent(short_data)
                    while short_data.len_in_seconds() < 2.5:
                        short_data += r

                segment_iterator = self._generate_wav_binary_iterator(short_data.to_bytes())
                vector = vr_stub.GetDvectorFromWav(segment_iterator).data
                matrix.append(vector)
                timestamp.append((segment.start, segment.end))

        matrix = np.stack(matrix, axis=0)
        labels = self.clusterer.predict(matrix)

        segments = list()
        for speaker, (start, end) in zip(labels.tolist(), timestamp):
            segments.append(Segment(
                speaker=speaker, start_time=start/sr, end_time=end/sr))

        return DiarizeResult(data=segments)

    def _generate_wav_binary_iterator(self, wav_binary):
        for idx in range(0, len(wav_binary), self.chunk_size):
            yield Speech(bin=wav_binary[idx:idx+self.chunk_size])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Diarization Server')
    parser.add_argument('-c', '--config', type=str, default='config/default.yaml',
                        help="path of configuration yaml file")
    parser.add_argument('-p', '--port', type=int, default=42002,
                        help="grpc: ip:port")
    parser.add_argument('-l', '--log_level', type=str, default='INFO')
    parser.add_argument('--stt_remote', type=str, default='127.0.0.1:15001')
    parser.add_argument('--vr_remote', type=str, default='127.0.0.1:43001')
    args = parser.parse_args()

    servicer = DiarizationImpl(args.stt_remote, args.vr_remote, args.config)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    add_DiarizeServicer_to_server(servicer, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )
    logging.info('Diarization (STT+VR pipeline) starting at 0.0.0.0:%d', args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
