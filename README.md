# brain_diarize

Pipeline brain-w2l & brain_vggsr for diarization.

Caveat: This diarization engine uses different proto definition from `dap.proto`, which was used within `brain-dap`.

## Build
```bash
docker build -f Dockerfile -t brain_diarize:<version> .
```

## Run

```bash
docker run -p 42002:42002 \
-e STT_REMOTE=<url:port> \
-e VR_REMOTE=<url:port> \
brain_diarize:<version>
```

## Author

브레인 박승원 수석, 김영현 연구원
