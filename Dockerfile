FROM nvcr.io/nvidia/pytorch:20.03-py3
RUN python -m pip --no-cache-dir install --upgrade \
        omegaconf==2.0.0 \
        grpcio==1.13.0 \
        grpcio-tools==1.13.0 \
        protobuf==3.6.0 \
        spectralcluster==0.1.0 \
        && \
mkdir /root/diarize/
COPY . /root/diarize/
RUN cd /root/diarize/ && \
    python -m grpc.tools.protoc --proto_path=brain-w2l/proto/maum/brain/w2l --python_out=. --grpc_python_out=. w2l.proto && \
    python -m grpc.tools.protoc --proto_path=brain_vggsr --python_out=. --grpc_python_out=. dap.proto && \
    python -m grpc.tools.protoc --proto_path=. --python_out=. --grpc_python_out=. diarize.proto
EXPOSE 42002
WORKDIR /root/diarize/
ENTRYPOINT python server.py --stt_remote $STT_REMOTE --vr_remote $VR_REMOTE
