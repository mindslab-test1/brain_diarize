import grpc
import argparse

from diarize_pb2 import Speech
from diarize_pb2_grpc import DiarizeStub


class DiarizeClient(object):
    def __init__(self, remote='127.0.0.1:42002', chunk_size=3145728):
        channel = grpc.insecure_channel(remote)
        self.stub = DiarizeStub(channel)
        self.chunk_size = chunk_size

    def diarize(self, message):
        message = self._generate_wav_binary_iterator(message)
        return self.stub.StreamDiarization(message)

    def _generate_wav_binary_iterator(self, wav_binary):
        for idx in range(0, len(wav_binary), self.chunk_size):
            yield Speech(bin=wav_binary[idx:idx+self.chunk_size])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Diarization Client')
    parser.add_argument('-r', '--remote', default='127.0.0.1:42002',
                        help="grpc: ip:port")
    parser.add_argument('-i', '--input', type=str, required=True,
                        help="input wav file")
    args = parser.parse_args()

    client = DiarizeClient(args.remote)

    with open(args.input, 'rb') as rf:
        wav_binary = rf.read()

    result = client.diarize(wav_binary)
    for segment in result.data:
        print(segment.speaker, segment.start_time, segment.end_time)
